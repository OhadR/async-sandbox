import * as log4js from 'log4js';
const LOG_LEVEL = 'debug';

log4js.configure({
  appenders: {
    out: {
      type: 'stdout',
      layout: {
        type: 'pattern',
        pattern: '%[[%d] [%p] %c%] %m',
      },
    },
  },
  categories: { 
    default: { appenders: ['out'], level: LOG_LEVEL }
  }
});
