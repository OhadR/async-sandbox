require('./logger');
import async from 'async';
import * as log4js from 'log4js';
const logger = log4js.getLogger('runner')

function sleep(ms: number) {
   return new Promise(resolve => setTimeout(resolve, ms));
}

function regularUsecase(callback) {
   logger.info('regularUsecase');
   async.waterfall([
      function (cb) {
         logger.info('[function 1]');
         cb(null, '1');
         // return '1';
      },
      function(objects: any, cb) {
         logger.info('[function 2]', objects);
         cb(null, '2');
      }], function(err: Error, results) {
         logger.info('[final handler]', err, results);
         callback(err, results);
   });
}

function regularUsecaseWithoutCallbacks(callback) {
   logger.info('regularUsecaseWithoutCallbacks');
   async.waterfall([
      async function () {           //NOTE: CRITICAL: without this 'async', the 'return' exits the waterfall !!
         logger.info('[function 1]');
         return '1';
      },
      function(objects: any, cb) {
         logger.info('[function 2]', objects);
         cb(null, '2');
      }], function(err: Error, results) {
      logger.info('[final handler]', err, results);
      callback(err, results);
   });
}

function regularUsecaseWithError(callback) {
   logger.info('regularUsecaseWithError');
   async.waterfall([
      function (cb) {
         logger.info('[function 1]');
         cb('1', '1');
         // return '1';
      },
      function(objects: any, cb) {
         logger.info('[function 2]', objects);
         cb(null, '2');
      }], function(err: Error, results) {
      logger.info('[final handler]', err, results);
      callback(err, results);
   });
}

function asyncUsecase(callback) {
   const functionName = 'asyncUsecase';
   logger.info(functionName);
   async.waterfall([
      async function (cb) {
         logger.info(`[${functionName} function 1]`);
         // cb(null, '1');    // TypeError: cb is not a function
         return '1';
      },
      function(objects: any, cb) {
         logger.info(`[${functionName} function 2]`, objects);
         cb(null, '2');
      }], function(err: Error, results) {
      logger.info(`[${functionName} final handler]`, err, results);
      callback(err, results);
   });
}

function asyncUsecaseWithError(callback) {
   const functionName = 'asyncUsecaseWithError';
   logger.info(functionName);
   async.waterfall([
      async function () {
         logger.info(`[${functionName} function 1]`);
         throw new Error('this is an err');
      },
      function(objects: any, cb) {
         logger.info(`[${functionName} function 2]`, objects);
         cb(null, '2');
      }],
      function(err: Error, results) {
         logger.info(`[${functionName} final handler]`, err, results);
         callback(err, results);
      }
   );
}

async function ohadsTest() {
   const functionName = 'ohads';
   logger.info(functionName);
   const waterfallRetVal = await async.waterfall([
      function (cb) {
         logger.info(`[${functionName} function 0]`);
         cb(null);
      },
      async function (cb) {
         await sleep(1000);
         logger.info(`[${functionName} function 1]`);
         // cb(null, '1');    // TypeError: cb is not a function
         return '1';
      },
      function (objects: any, cb) {
         logger.info(`[${functionName} function 2]`, objects);
         cb(null, {a: '2', b: 'd',});
      }]
   );
   logger.info('waterfallRetVal', waterfallRetVal);
   return waterfallRetVal;
}

async function run() {
   // regularUsecase(function(err, data) {
   //    logger.info('[regularUsecase]', data);
   // });

   regularUsecaseWithoutCallbacks(function(err, data) {
      logger.info('[regularUsecaseWithoutCallbacks]', data);
   });

   // regularUsecaseWithError(function(err, data) {
   //    logger.info('[regularUsecaseWithError]', data);
   // });
   //
   // awaitUsecase(function(err, data) {
   //    logger.info('[awaitUsecase]', data);
   // });

   try {
      const {a, b} = await ohadsTest();

      logger.info('ohads result:', a, b);
   } catch (e) {
      logger.error(e);
   }
}



run().then(() => {
   logger.info('done all');
   process.exit(0)
}, e => {
   console.error(e); process.exit(1)
})

function foo(a: number, b: number) {
   const sum = a + b;
   const mult = a * b;
   const div = a / b;
   return {sum, mult, div};
}